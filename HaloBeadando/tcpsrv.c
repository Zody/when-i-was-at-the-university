/***************************************/
/*              TCP server             */
/***************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h> 
#include <unistd.h>

#define BUFSIZE 1024                      // buffer size
#define PORT_NO 2001                      // port number
#define error(a,b) fprintf(stderr, a, b)  // error 'function'

int ertek(char a[30], int sum, int lapszam){
	int i;
	for (i = 0; i < lapszam*2; i = i + 2){
		if (a[i] == 'A'){
			sum = sum + 2;	
		} 
		else if (a[i] == 'F'){
			sum = sum + 3;	
			}
			else if (a[i] == 'K'){
				sum = sum + 4;
				}
				else if (a[i] == 'Q'){
					sum = sum + 11;
					}
					else if (a[i] == '7'){
							sum = sum + 7;
						}
						else if (a[i] == '8'){
								sum = sum + 8;
							}
							else if (a[i] == '9'){
									sum = sum + 9;
								}
								else if (a[i] == 'X'){
										sum = sum + 10;
									}
	}
	return sum;
}

int main(int argc, char *argv[] ){ 	// arg count, arg vector 

   /* Declarations */
   int fd;	        	           		// socket endpt
   int fdc;
   int fdc2;                        	// socket endpt
   int flags;                      	// rcv flags
   struct sockaddr_in server;      	// socket name (addr) of server
   struct sockaddr_in client;
   struct sockaddr_in client2;	     	// socket name of client
   int server_size;                	// length of the socket addr. server
   int client_size;                	// length of the socket addr. client
   int bytes;		           					// length of buffer 
   int bytes2;
   int rcvsize;                    	// received bytes
   int trnmsize;                   	// transmitted bytes
   int err;                        	// error code
   char on;                        	// 
   char buffer[BUFSIZE+1];	     		// datagram dat buffer area   
   char buffer2[BUFSIZE+1];
   int over;
   char c_values[] = {'7','8','9','X','A','F','K','Q'};
   char c_types[] = {'P','M','T','Z'};
   int again = 1;
   int feladva = 0;
   int win1, win2;

   /* Initialization */
   on                     = 1;
   flags                  = 0;
   bytes                  = BUFSIZE;
   server_size            = sizeof server;
   client_size            = sizeof client;
   server.sin_family      = AF_INET;
   server.sin_addr.s_addr = INADDR_ANY;
   server.sin_port        = htons(PORT_NO);
   over 		  = 0;

   /* Creating socket */
   fd = socket(AF_INET, SOCK_STREAM, 0 );
   if (fd < 0) {
      error("%s: Socket creation error\n",argv[0]);
      exit(1);
      }

   /* Setting socket options */
   setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on);
   setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)&on, sizeof on);
  
   /* Binding socket */
   err = bind(fd, (struct sockaddr *) &server, server_size);
   if (err < 0) {
      error("%s: Cannot bind to the socket\n",argv[0]);
      exit(2);
      }

   /* Listening */
   err = listen(fd, 10);
   if (err < 0) {
      error("%s: Cannot listen to the socket\n",argv[0]);
      exit(3);
      }

// ------------------------------------------

   /* Accepting connections request */
   fdc = accept(fd, (struct sockaddr *) &client, &client_size);
   if (fdc < 0) {
      error("%s: Cannot accept on socket\n",argv[0]);
      exit(5);
      }

// ------------------------------------------

   /*Writing to client 1 */

   read(fdc, buffer, BUFSIZE);
   sprintf(buffer,"Waiting for one more player...\n");
   bytes = strlen(buffer)+1;
   write(fdc, buffer, bytes);
   read(fdc, buffer, BUFSIZE);

// -------------------------------------------

   /* Accept connection request 2. */
   fdc2 = accept(fd, (struct sockaddr *) &client2, &client_size);
   if (fdc2 < 0) {
      error("%s: Cannot accept on socket\n",argv[0]);
      exit(5);
      }

// -------------------------------------------

   read(fdc2, buffer2, BUFSIZE);
   sprintf(buffer2, "OK");
   bytes2 = strlen(buffer2)+1;
   write(fdc2, buffer2, strlen(buffer2));

   read(fdc2, buffer2, BUFSIZE);
   

// ---------------------------------------------
	/* GAME */
   srand(time(0));
   int r,i, sum_p1, sum_p2;
   char sendcards_p1[30];
   char sendcards_p2[30];
   int seged;
	

   while (again == 1){
	/*Lapok inicializálása */
	
		for (i = 0; i < 30; i++){
			sendcards_p1[i] = '\0';
			sendcards_p2[i] = '\0';
		}
		/* Cards for first player */	
		r = rand() % 8;
		sendcards_p1[0] = c_values[r];
		r = rand() % 4;
		sendcards_p1[1] = c_types[r];
		r = rand() % 8;
		sendcards_p1[2] = c_values[r];
		r = rand() % 4;
		sendcards_p1[3] = c_types[r];

	

		/* Cards for second player */	
		r = rand() % 8;
		sendcards_p2[0] = c_values[r];
		r = rand() % 4;
		sendcards_p2[1] = c_types[r];
		r = rand() % 8;
		sendcards_p2[2] = c_values[r];
		r = rand() % 4;
		sendcards_p2[3] = c_types[r];
	


	/* Lapok értékeinek kiszámítása */

	sum_p1 = 0;
	sum_p1 = ertek(sendcards_p1, 0, 2);
	sum_p2 = 0;
	sum_p2 = ertek(sendcards_p2, 0, 2);
	
	sendcards_p1[4] = '\0';
	printf("sendcards_p1 = %s, it's value = %d\n", sendcards_p1, sum_p1);


	sendcards_p2[4] = '\0';

	printf("sendcards_p2 = %s, it's value = %d\n", sendcards_p2, sum_p2);

	/* Kezdő üzenet kiküldése */

	sprintf(buffer, "\n ===== The game has been started. ===== \n\n");
	   bytes = strlen(buffer)+1;
	   write(fdc, buffer, bytes);
	   read(fdc, buffer, BUFSIZE);

	sprintf(buffer2, "\n ===== The game has been started. =====\n\n");
	   bytes2 = strlen(buffer2)+1;
	   write(fdc2, buffer2, bytes2);
	   
	   read(fdc2, buffer2, BUFSIZE);

	/* Lapok kiosztása */

	bytes = strlen(sendcards_p1)+1;
	write(fdc, sendcards_p1, bytes);
	read(fdc, buffer, BUFSIZE);
	
	bytes2 = strlen(sendcards_p2)+1;
	write(fdc2, sendcards_p2, bytes2);
	read(fdc2, buffer2, BUFSIZE);

	bytes = strlen(sendcards_p2)+1;
	write(fdc, sendcards_p2, bytes);
	read(fdc, buffer, BUFSIZE);

	bytes2 = strlen(sendcards_p1)+1;
	write(fdc2, sendcards_p1, bytes2);
	read(fdc2, buffer2, BUFSIZE);

	/* Lapértékek kiküldése */

	

	int tmp;
	tmp = htonl(sum_p1);
	write(fdc, &tmp, sizeof(tmp));
	read(fdc, buffer, BUFSIZE);

	tmp = htonl(sum_p2);
	write(fdc, &tmp, sizeof(tmp));
	read(fdc, buffer, BUFSIZE);

	tmp = htonl(sum_p2);
	write(fdc2, &tmp, sizeof(tmp));
	read(fdc2, buffer2, BUFSIZE);
	
	tmp = htonl(sum_p1);
	write(fdc2, &tmp, sizeof(tmp));
	read(fdc2, buffer2, BUFSIZE);

	 
	
	/* Kérdések kiküldése a klienseknek, hogy mennyi lapot kérnek még. */
	int dec1, dec2;

	sprintf(buffer, "How many cards do you want to draw?: ");
	bytes = strlen(buffer)+1;
	write(fdc, buffer, bytes);
	read(fdc, buffer, BUFSIZE);

	sprintf(buffer, "OK");
	bytes = strlen(buffer)+1;
	write(fdc, buffer, bytes);
	read(fdc, buffer, BUFSIZE);
	dec1 = atoi(buffer);
	printf("%d erkezett\n", dec1);	


	sprintf(buffer2, "How many cards do you want to draw?: ");
	bytes2 = strlen(buffer2)+1;
	write(fdc2, buffer2, bytes2);
	read(fdc2, buffer2, BUFSIZE);

	sprintf(buffer2, "OK");
	bytes2 = strlen(buffer2)+1;
	write(fdc2, buffer2, bytes2);
	read(fdc2, buffer2, BUFSIZE);
	dec2 = atoi(buffer2);
	printf("%d erkezett\n", dec2);	
	
	if (strcmp(buffer, "feladom") == 0 && strcmp(buffer2, "feladom") != 0) {
		win1 = 0;
		win2 = 1;
		printf("1. KLIENS FELADTA\n");
		
		sprintf(buffer, "feladva");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);

		sprintf(buffer2, "feladva");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes2);
		read(fdc2, buffer2, BUFSIZE);

		sprintf(buffer, "You forfeited. ");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);

		sprintf(buffer2, "The other player forfeited ");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes2);
		read(fdc2, buffer2, BUFSIZE);
	}
	else if (strcmp(buffer, "feladom") != 0 && strcmp(buffer2, "feladom") == 0) {
			win1 = 1;
			win2 = 0;
			printf("2. KLIENS FELADTA\n");

			sprintf(buffer, "feladva");
			bytes = strlen(buffer)+1;
			write(fdc, buffer, bytes);
			read(fdc, buffer, BUFSIZE);

			sprintf(buffer2, "feladva");
			bytes2 = strlen(buffer2)+1;
			write(fdc2, buffer2, bytes2);
			read(fdc2, buffer2, BUFSIZE);

			sprintf(buffer, "The other player forfeited ");
			bytes = strlen(buffer)+1;
			write(fdc, buffer, bytes);
			read(fdc, buffer, BUFSIZE);

			sprintf(buffer2, "You forfeited ");
			bytes2 = strlen(buffer2)+1;
			write(fdc2, buffer2, bytes2);
			read(fdc2, buffer2, BUFSIZE);
		}
		else if (strcmp(buffer, "feladom") == 0 && strcmp(buffer2, "feladom") == 0) {
				win1 = 0;
				win2 = 0;
				printf("MINDKET KLIENS FELADTA\n");
		
				sprintf(buffer, "feladva");
				bytes = strlen(buffer)+1;
				write(fdc, buffer, bytes);
				read(fdc, buffer, BUFSIZE);

				sprintf(buffer2, "feladva");
				bytes2 = strlen(buffer2)+1;
				write(fdc2, buffer2, bytes2);
				read(fdc2, buffer2, BUFSIZE);

				sprintf(buffer, "You and your opponent both forfeited. ");
				bytes = strlen(buffer)+1;
				write(fdc, buffer, bytes);
				read(fdc, buffer, BUFSIZE);

				sprintf(buffer2, "You and your opponent both forfeited. ");
				bytes2 = strlen(buffer2)+1;
				write(fdc2, buffer2, bytes2);
				read(fdc2, buffer2, BUFSIZE);
			}
			else if (strcmp(buffer, "feladom") != 0 && strcmp(buffer2, "feladom") != 0){
	
	sprintf(buffer, "OK");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);

	sprintf(buffer2, "OK");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes2);
		read(fdc2, buffer2, BUFSIZE);

	/* Döntés kezelése 1. client */
	char newcards[30];
	for (i = 0; i < dec1*2; i = i + 2){
		r = rand() % 8;
		newcards[i] = c_values[r];
		r = rand() % 4;
		newcards[i + 1] = c_types[r];
	}
	strcat(sendcards_p1, newcards);
	sendcards_p1[dec1*2 + 4 + 1] = '\0';
	sum_p1 = ertek(sendcards_p1, 0, 2 + dec1);

	/* Döntés kezelése 2. client */
	char newcards2[30];
	for (i = 0; i < dec2*2; i = i + 2){
		r = rand() % 8;
		newcards2[i] = c_values[r];
		r = rand() % 4;
		newcards2[i + 1] = c_types[r];
	}
	strcat(sendcards_p2, newcards2);
	sendcards_p2[dec2*2 + 4 + 1] = '\0';
	sum_p2 = ertek(sendcards_p2, 0, 2 + dec2);


	
	/* Új eredmények kiküldése a klienseknek */
	
	sprintf(buffer, sendcards_p1);
	bytes = strlen(buffer)+1;
	write(fdc, buffer, bytes);
	read(fdc, buffer, BUFSIZE);

	sprintf(buffer, sendcards_p2);
	bytes = strlen(buffer)+1;
	write(fdc, buffer, bytes);
	read(fdc, buffer, BUFSIZE);

	
	sprintf(buffer2, sendcards_p2);
	bytes2 = strlen(buffer2)+1;
	write(fdc2, buffer2, bytes2);
	read(fdc2, buffer2, BUFSIZE);

	sprintf(buffer2, sendcards_p1);
	bytes2 = strlen(buffer2)+1;
	write(fdc2, buffer2, bytes2);
	read(fdc2, buffer2, BUFSIZE);



	tmp = htonl(sum_p1);
	write(fdc, &tmp, sizeof(tmp));
	read(fdc, buffer, BUFSIZE);
	
	tmp = htonl(sum_p2);
	write(fdc, &tmp, sizeof(tmp));
	read(fdc, buffer2, BUFSIZE);


	tmp = htonl(sum_p2);
	write(fdc2, &tmp, sizeof(tmp));
	read(fdc2, buffer, BUFSIZE);
	
	tmp = htonl(sum_p1);
	write(fdc2, &tmp, sizeof(tmp));
	read(fdc2, buffer2, BUFSIZE);
	
	
	/* Eredmény meghatározása, és kiküldése */
	
	if (sum_p1 > 21) {
		win1 = 0;
		sprintf(buffer, "You busted");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);
	}
	else {
		win1 = 1;
		sprintf(buffer, "The result is: ");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);
	}
	

	if (sum_p2 > 21) {
		win2 = 0;
		sprintf(buffer2, "You busted");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes);
		read(fdc2, buffer2, BUFSIZE);
	}
	else {
		win2 = 1;
		sprintf(buffer2, "The result is: ");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes);
		read(fdc2, buffer2, BUFSIZE);
	}
	}

	if (win1 == 0 && win2 == 0){
		sprintf(buffer, "Meh, it's a draw... :^|\n");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);

		sprintf(buffer2, "Meh, it's a draw... :^|\n");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes2);
		read(fdc2, buffer2, BUFSIZE);
	}
	else
	if (win1 == 1 && win2 == 0) {
		sprintf(buffer, "Congratulations. You won the game! :^)\n");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);

		sprintf(buffer2, "Unfortunate... You lost the game :^(\n");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes2);
		read(fdc2, buffer2, BUFSIZE);
	}
	else if (win1 == 0 && win2 == 1){
			sprintf(buffer, "Unfortunate... You lost the game :^(\n");
			bytes = strlen(buffer)+1;
			write(fdc, buffer, bytes);
			read(fdc, buffer, BUFSIZE);

			sprintf(buffer2, "Congratulations. You won the game! :^)\n");
			bytes2 = strlen(buffer2)+1;
			write(fdc2, buffer2, bytes2);
			read(fdc2, buffer2, BUFSIZE);
		}
		else if (win1 == 1 && win2 == 1){
				if (sum_p1 == sum_p2){
					sprintf(buffer, "Meh, it's a draw... :^|\n");
					bytes = strlen(buffer)+1;
					write(fdc, buffer, bytes);
					read(fdc, buffer, BUFSIZE);

					sprintf(buffer2, "Meh, it's a draw... :^|\n");
					bytes2 = strlen(buffer2)+1;
					write(fdc2, buffer2, bytes2);
					read(fdc2, buffer2, BUFSIZE);
				}
				else if (sum_p1 > sum_p2){
						sprintf(buffer, "Congratulations. You won the game! :^)\n");
						bytes = strlen(buffer)+1;
						write(fdc, buffer, bytes);
						read(fdc, buffer, BUFSIZE);

						sprintf(buffer2, "Unfortunate... You lost the game :^(\n");
						bytes2 = strlen(buffer2)+1;
						write(fdc2, buffer2, bytes2);
						read(fdc2, buffer2, BUFSIZE);
					}
					else {
						sprintf(buffer, "Unfortunate... You lost the game :^(\n");
						bytes = strlen(buffer)+1;
						write(fdc, buffer, bytes);
						read(fdc, buffer, BUFSIZE);

						sprintf(buffer2, "Congratulations. You won the game! :^)\n");
						bytes2 = strlen(buffer2)+1;
						write(fdc2, buffer2, bytes2);
						read(fdc2, buffer2, BUFSIZE);
					}
			}
	
	
	/* Új jácma kérdés kiküldése */

	sprintf(buffer, "Would you like to play again? (yes/no): ");
	bytes = strlen(buffer)+1;
	write(fdc, buffer, bytes);
	read (fdc, buffer, BUFSIZE);

	sprintf(buffer2, "Would you like to play again? (yes/no): ");
	bytes2 = strlen(buffer2)+1;
	write(fdc2, buffer2, bytes);
	read (fdc2, buffer2, BUFSIZE);

	if (buffer[0] == 'n' || buffer2[0] == 'n'){
		again = 0;
		
		/* Új jácma döntés elküldése */
		sprintf(buffer, "no");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);
		
		sprintf(buffer2, "no");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes2);
		read(fdc2, buffer2, BUFSIZE);
	}
	else {
		sprintf(buffer, "yes");
		bytes = strlen(buffer)+1;
		write(fdc, buffer, bytes);
		read(fdc, buffer, BUFSIZE);
		
		sprintf(buffer2, "yes");
		bytes2 = strlen(buffer2)+1;
		write(fdc2, buffer2, bytes2);
		read(fdc2, buffer2, BUFSIZE);
	}
   }
// ---------------------------------------------

   /* Closing sockets and quit */
	usleep(10);
   close(fd);
   close(fdc);
   close(fdc2);
   exit(0);
} 
