/***************************************/
/*             TCP client              */
/***************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFSIZE 1024
#define PORT_NO 2001
#define error(a,b) fprintf(stderr, a, b)

struct card{
	char value;
	char type;
};

int main(int argc, char *argv[] ) {// arg count, arg vector   

   /* Declarations */
   int fd;	                       // socket endpt	
   int flags;                      // rcv flags
   struct sockaddr_in server;	     // socket name (addr) of server 
   struct sockaddr_in client;	     // socket name of client 
   int server_size;                // length of the socket addr. server 
   int client_size;                // length of the socket addr. client 
   int bytes;    	                 // length of buffer 
   int rcvsize;                    // received bytes
   int trnmsize;                   // transmitted bytes
   int err;                        // error code
   int ip;												 // ip address
   char on;                        // 
   char buffer[BUFSIZE+1];         // datagram dat buffer area
   char server_addr[16];           // server address	
   char message[BUFSIZE+1];
   int over;
   int again = 1;
   int feladva = 0;

   /* Initialization */
   on    = 1;
   flags = 0;
   server_size = sizeof server;
   client_size = sizeof client;
   sprintf(server_addr, "%s", argv[1]);
   ip = inet_addr(server_addr);
   server.sin_family      = AF_INET;
   server.sin_addr.s_addr = ip;
   server.sin_port        = htons(PORT_NO);
   over	= 0;

   /* User interface */
   printf("Welcome to the 21 card game!\n");
	   bytes = strlen(buffer)+1;

	   /* Creating socket */
	   fd = socket(AF_INET, SOCK_STREAM, 0);
	   if (fd < 0) {
	      error("%s: Socket creation error.\n",argv[0]);
	      exit(1);
	   }

	   /* Setting socket options */
	   setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on);
	   setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)&on, sizeof on);

	   /* Connecting to the server */
	   err = connect(fd, (struct sockaddr *) &server, server_size);
	   if (err < 0) {
	      error("%s: Cannot connect to the server.\n", argv[0]);
	      exit(2);
	   }

	   sprintf(buffer, "OK");
	   bytes = strlen(buffer)+1;
	   write(fd, buffer, bytes);

	   read(fd, buffer, BUFSIZE);
		   if (strcmp(buffer, "Waiting for one more player...\n") == 0){
			printf("%s", buffer);
		   }
		   sprintf(buffer, "OK");
		   bytes = strlen(buffer)+1;
		   write(fd, buffer, bytes);

// -----------------------------------------------
   while (again == 1){
	/* Kezdőüzenet fogadása */

	   read(fd, buffer, BUFSIZE);
		   printf("%s", buffer);
		   sprintf(buffer, "OK");
		   bytes = strlen(buffer)+1;
		   write(fd, buffer, bytes);

	/* Lapok fogadása */
	
	char mycards[30];
	char opcards[30];
	int i;

	for (i = 0; i < 30; i++){
		mycards[i] = '\0';
		opcards[i] = '\0';
	}

	   read(fd, buffer, BUFSIZE);
		   sprintf(mycards, buffer);

		   sprintf(buffer, "OK");
		   bytes = strlen(buffer)+1;
		   write(fd, buffer, bytes);

	   read(fd, buffer, BUFSIZE);
		   sprintf(opcards, buffer);

		   sprintf(buffer, "OK");
		   bytes = strlen(buffer)+1;
		   write(fd, buffer, bytes);

	/* Lapértékek fogadása */
	
	int mysum, opsum;
	int tmp;

	   read(fd, &tmp, sizeof(tmp));
		mysum = htonl(tmp);
		sprintf(buffer, "OK");
		bytes = strlen(buffer)+1;
		write(fd, buffer, bytes);

	   read(fd, &tmp, sizeof(tmp));
		opsum = htonl(tmp);
		sprintf(buffer, "OK");
		bytes = strlen(buffer)+1;
		write(fd, buffer, bytes);

	/* Játékállás közlése */

	   printf("Your cards are: %s and their value is %d\nYour opponent's cards are: %s, and their value is: %d\n", mycards, mysum, opcards, opsum);

	
	/* Kérdés fogadása, döntés beolvasása */
	char dec[10];
	read(fd, buffer, BUFSIZE);
		printf("%s", buffer);
		sprintf(buffer, "OK");
		bytes = strlen(buffer)+1;
		write(fd, buffer, bytes);

	read(fd, buffer, BUFSIZE);
		scanf("%s", dec);
		sprintf(buffer, dec);
		bytes = strlen(buffer)+1;
	/* Döntés elküldése a szervernek */
		
	write(fd, buffer, bytes);


	read(fd, buffer, BUFSIZE);
	if (strcmp(buffer, "feladva") != 0){
		sprintf(buffer, "OK");
		bytes = strlen(buffer)+1;
		write(fd, buffer, bytes);

	

	/* Új eredmények fogadása, és kiírása */
	
		read(fd, buffer, BUFSIZE);
			sprintf(mycards, buffer);
			sprintf(buffer, "OK");
			bytes = strlen(buffer)+1;
			write(fd, buffer, bytes);

		read(fd, buffer, BUFSIZE);
			sprintf(opcards, buffer);
			sprintf(buffer, "OK");
			bytes = strlen(buffer)+1;
			write(fd, buffer, bytes);

		read(fd, &tmp, sizeof(tmp));
			mysum = htonl(tmp);
			write(fd, buffer, bytes);

		read(fd, &tmp, sizeof(tmp));
			opsum = htonl(tmp);
			write(fd, buffer, bytes);
		
		printf("Your new cards are: %s, their value is %d\n Your opponent's new cards are: %s, their value is %d\n", mycards, mysum, opcards, opsum);

	}
	else{
		sprintf(buffer, "OK");
			bytes = strlen(buffer)+1;
			write(fd, buffer, bytes);
	}
	/* Eredmény fogadása, és kiírása */
	char be;
	read(fd, buffer, BUFSIZE);
		printf("%s\n", buffer);
		sprintf(buffer, "OK");
		bytes = strlen(buffer)+1;
		write(fd, buffer, bytes);
	
	read(fd, buffer, BUFSIZE);
		printf("%s\n", buffer);
		sprintf(buffer, "OK");
		bytes = strlen(buffer)+1;
		write(fd, buffer, bytes);

	/* Új jácma kérdés fogadása */
	
	read(fd, buffer, BUFSIZE);
		printf("%s", buffer);
		scanf("%s", buffer);
		bytes = strlen(buffer);
		write(fd, buffer, bytes);
	
	/* Új jácma döntés fogadása */
	read(fd, buffer, BUFSIZE);
		if (buffer[0] == 'n') again = 0;
		sprintf(buffer, "OK");
		bytes = strlen(buffer)+1;
		write(fd, buffer, bytes);
   }
// -----------------------------------------------------

	   /* Closing sockets and quit */
   close(fd);

   exit(0);
} 
